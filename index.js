/**
 * @NodeJS_Version 16.1.0
 */

const dotenv = require("dotenv").config()
if (dotenv.error) {
    console.log(dotenv.parsed)
    throw dotenv.error
}

const port = process.env.PORT || 3000

const log = console.log

const express = require("express")
const app = express()
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

require('./src/spreadsheet.js')(app)
require('./src/enotas.js')(app)


app.get("/", (req, res) => {
    res.status(200).json({
        status: "API Intermediária Bebilíngue v1.0.3",
    })
})

app.listen(port, () => {
    log(`Server is up on port ${port}`)
})
