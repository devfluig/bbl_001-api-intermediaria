
const axios = require('axios')
const { dateToday, parse } = require('./util.js')

const log = console.log

const baseURL = 'https://app.enotas.com.br/api'
const ENOTAS_AUTH = process.env.ENOTAS_AUTH

const ENotas = (app) => {

    /**
     * Retorna JSON com ID do cliente (clienteId).
     * (!) Guardar o id do cliente em "extras".
     */
    app.post('/enotas/clientes', (req, res) => {
        const { email, nome, cpfCnpj } = req.body
        const data = req.body

        if(!email || !nome || !cpfCnpj) return res.status(400).send({
            status: 400,
            error: "Missing one of the following required parameters: 'email', 'nome', 'cpfCnpj'"
        })

        return axios({
            method: "post",
            url: baseURL + '/clientes',
            headers: {
                Authorization: ENOTAS_AUTH
            },
            data: data
        })
        .then( response => {
            response = response.data
            if (!response.clienteId)
                return res.status(500).send({ error: "Creation failed.", status: 500, description: response })

            res.status(201).send(response)
        })
        .catch( err => {
            res.status(500).send({ error: "Internal Server Error: " + err, status: 500})
        })

    })

    /**
     * Retorna Array com IDs das vendas.
     */
    app.post('/enotas/vendas', async (req, res) => {

        const postVenda = (vendaObj) => {
            return axios({
                method: "post",
                url: baseURL + '/vendas',
                headers: {
                    Authorization: ENOTAS_AUTH
                },
                data: vendaObj
            })
            .then( response => {
                response = response.data
                if (!response.vendaId)
                    return { error: "Creation failed.", status: 500, description: response, vendasId: response }
                //log('Response:', response)
                return response
            })
            .catch( err => {
                return { error: "Internal Server Error: " + err, status: 500, vendasId: err}
            })
        }

        let dataBody = req.body

        const today = dateToday()
        const diaMesAno = `${parse(today.getDate())}/${parse(today.getMonth() + 1)}/${today.getFullYear()}`

        dataBody.data = diaMesAno  // Insere data de hoje.
        dataBody.dataCompetencia = diaMesAno

        const produtos = dataBody.produtos
        dataBody.meioPagamento = parseInt(dataBody.meioPagamento)
        delete dataBody.produtos

        const vendas = await Promise.all(produtos.map(async (prod) => {
            let mensagemPadrao = 'DOCUMENTO EMITIDO POR ME OU EPP OPERANTE PELO SIMPLES NACIONAL. NÃO GERA DIREITO A CRÉDITO FISCAL DE IPI \n'
            mensagemPadrao += 'Nome do aluno: ' + prod.aluno + ' - COLÉGIO: ' + prod.colegio
            try {
                const addr = prod.endereco
                mensagemPadrao += `\n ${addr.rua}, no. ${addr.numeroCasa}, ${addr.complemento}, ${addr.bairro}, ${addr.cidade} - ${addr.cep}`
            } catch (err) { }

            dataBody.produto = {}
            dataBody.produto.nome = prod.nome
            dataBody.produto.valorTotal = parseFloat(String(prod.valor).replace(',', '.'))
            dataBody.valorTotal = dataBody.produto.valorTotal
            dataBody.valorTotalNFe = dataBody.produto.valorTotal
            dataBody.discriminacao = prod.nome
            dataBody.observacoes = mensagemPadrao
            
            let tentativas = 0
            let vendaId = null

            // console.log(dataBody)

            while(tentativas < 3) {  // Às vezes volta status 500 por nenhuma razão.
                //log('2:', dataBody.produto)
                vendaId = await postVenda(dataBody)
                tentativas++
                if(vendaId.vendaId) break
            }

            return vendaId.vendaId
        }))

        // console.log('Venda realizada:', prod.nome, vendas)
        res.status(201).send(vendas)
    })

}

module.exports = ENotas
