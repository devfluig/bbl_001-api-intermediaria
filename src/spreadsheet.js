/**
 * Endpoint para integração com Google Sheets.
 */

const { GoogleSpreadsheet } = require('google-spreadsheet')
const { dateToday, parse } = require('./util.js')

const accessSpreadsheet = async(ind = 0) => {
    const doc = new GoogleSpreadsheet(process.env.GOOGLE_SPREADSHEET_ID)
    await doc.useServiceAccountAuth({
        client_email: process.env.GOOGLE_SERVICE_ACCOUNT_EMAIL,
        private_key: process.env.GOOGLE_PRIVATE_KEY,
    })
    await doc.loadInfo()
    const sheet = doc.sheetsByIndex[ind]

    return sheet
}

const addRow = async(data, sheet) => {
    return await sheet.addRow(data)
}

const googleSheets = async (app) => {
    const doc = await accessSpreadsheet()

    app.post('/googlesheets/addrow', async (req, res) => {
        const data = req.body

        /**
         * Inclui data e horário.
         */
        const today = dateToday()
        data.Data = `${parse(today.getDate())}/${parse(today.getMonth() + 1)}/${today.getFullYear()} | ${parse(today.getHours())}:${parse(today.getMinutes())}:${parse(today.getSeconds())}`

        try {
            const responseRow = await addRow(data, doc)
            if (responseRow)
                return res.status(201).send({
                    status: 201,
                    message: 'Created'
                })

            res.status(401).send({
                status: 401,
                message: 'Bad request'
            })
        } catch (err) {
            console.error(err)
            res.status(500).send({
                status: 500,
                error: err
            })
        }
    })

}


module.exports = googleSheets
