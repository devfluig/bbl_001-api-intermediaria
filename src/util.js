
const dateToday = (GMT = -3) => {
    const dataGMTBrasilia = new Date()
    const offset = (dataGMTBrasilia.getTimezoneOffset() + GMT * 60) * 60000    
    let today = dataGMTBrasilia.getTime()
    today = new Date(today + offset)

    return today
}

const parse = (num) => {
    num = String(num)
    if(num.length < 2) num = '0' + num
    return num
}


module.exports = {
    dateToday,
    parse
}
